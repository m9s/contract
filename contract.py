#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import Equal, Eval, Not, In
from trytond.transaction import Transaction
from trytond.pool import Pool


_STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft'))
    }
_DEPENDS = ['state']
_CONTRACT_STATES = [
    ('draft', 'Draft'),
    ('active', 'Active'),
    ('terminated', 'Terminated'),
    ('cancel', 'Canceled')
    ]


class ContractCategory(ModelSQL, ModelView):
    'Contract Category'
    _name = 'contract.category'
    _description = __doc__

    name = fields.Char('Name', required=True)
    parent = fields.Many2One('contract.category', 'Parent')
    childs = fields.One2Many('contract.category', 'parent',
            string='Children')

    def __init__(self):
        super(ContractCategory, self).__init__()
        self._order.insert(0, ('name', 'ASC'))

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        def _name(category):
            if category.id in res:
                return res[category.id]
            elif category.parent:
                return _name(category.parent) + ' / ' + category.name
            else:
                return category.name
        for category in self.browse(ids):
            res[category.id] = _name(category)
        return res

ContractCategory()


class Contract(ModelWorkflow, ModelSQL, ModelView):
    'Contract'
    _name = 'contract.contract'
    _description = __doc__

    name = fields.Char('Name', required=True, states={
            'readonly': In(Eval('state'), ['cancel', 'terminated']),
        }, depends=['state'])
    code = fields.Char('Code', readonly=True)
    code_external = fields.Char('External Code', states={
            'readonly': In(Eval('state'), ['cancel', 'terminated']),
        }, depends=['state'])
    parties = fields.One2Many('contract.party', 'contract',
            'Contracting Parties', required=True, states=_STATES,
            depends=_DEPENDS)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=_STATES, depends=_DEPENDS)
    state = fields.Selection(_CONTRACT_STATES, 'State', required=True,
            readonly=True)
    category = fields.Many2One('contract.category', 'Category', states={
            'readonly': In(Eval('state'), ['cancel', 'terminated']),
            }, depends=['state'])
    manager = fields.Many2One('company.employee', 'Contract Manager',
            domain=[('company', '=', Eval('company'))], states={
            'readonly': In(Eval('state'), ['cancel', 'terminated']),
        }, depends=['company', 'state'])
    sign_date = fields.Date('Sign Date', states=_STATES, depends=_DEPENDS)
    start_date = fields.DateTime('Start Date', required=True, states=_STATES,
        depends=_DEPENDS)
    end_date = fields.DateTime('End Date', states={
            'readonly': In(Eval('state'), ['cancel', 'terminated']),
        }, depends=['state'])
    description = fields.Text('Description', states={
            'readonly': In(Eval('state'), ['cancel', 'terminated']),
        }, depends=['state'])

    def __init__(self):
        super(Contract, self).__init__()
        self._constraints += [
            ('check_end_date', 'early_end_date'),
        ]
        self._error_messages.update({
            'early_end_date': 'The end date of a contract must be after '
                    'its start date.',
            'late_end_date': 'A contract can only be terminated after '
                    'reaching its end date! Affected contract: %s (%s)',
            })

    def default_start_date(self):
        date_obj = Pool().get('ir.date')
        # Return today at 00:00 according to users timezone in server time
        now = datetime.datetime.now()
        now = datetime.datetime.combine(now.date(), datetime.time.min)
        user = Transaction().user
        now = date_obj.user2localtime(user, now)
        return now

    def default_company(self):
        return Transaction().context.get('company') or False

    def default_state(self):
        return 'draft'

    def create(self, values):
        sequence_obj = Pool().get('ir.sequence')
        values = values.copy()
        if not values.get('code'):
            values['code'] = sequence_obj.get('contract.contract')
        return super(Contract, self).create(values,)

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['code'] = False
        return super(Contract, self).copy(ids, default=default)

    def check_end_date(self, ids):
        for contract in self.browse(ids):
            if contract.end_date and contract.end_date < contract.start_date:
                return False
        return True

    def check_terminate(self, contract_id):
        # contract end_date must be (before) now to *terminate* a contract
        contract = self.browse(contract_id)
        now = datetime.datetime.now()
        res = False
        if contract.end_date and contract.end_date <= now:
            res = True
        else:
            self.raise_user_error('late_end_date', error_args=(contract.name,
                contract.code))
        return res

    def check_cancel(self, contract_id):
        # Use this method to check in dependent modules for opened legal
        # documents related to the contract
        return True

    def wkf_contract_activity_draft(self, contract):
        self.write(contract.id, {'state': 'draft'})

    def wkf_contract_activity_activate(self, contract):
        self.write(contract.id, {'state': 'active'})

    def wkf_contract_activity_cancel(self, contract):
        self.write(contract.id, {'state': 'cancel'})

    def wkf_contract_activity_terminate(self, contract):
        self.write(contract.id, {'state': 'terminated'})

    def wkf_contract_transition_draft_cancel(self, contract):
        return self.check_cancel(contract.id)

    def wkf_contract_transition_activate_cancel(self, contract):
        return self.check_cancel(contract.id)

    def wkf_contract_transition_activate_terminate(self, contract):
        return self.check_terminate(contract.id)

Contract()


class ContractParty(ModelSQL, ModelView):
    'Contract Party'
    _name = 'contract.party'
    _description = __doc__
    _rec_name = 'party'

    party = fields.Many2One('party.party', 'Party', required=True)
    contract = fields.Many2One('contract.contract', 'Contract', required=True,
            ondelete='CASCADE')

ContractParty()

