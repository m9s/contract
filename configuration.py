# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Contract Configuration'
    _name = 'contract.configuration'
    _description = __doc__

    contract_sequence = fields.Property(fields.Many2One('ir.sequence',
        'Contract Sequence', domain=[
            ('company', 'in', [Eval('company'), False]),
            ('code', '=', 'contract.contract'),
        ], required=True, depends=['company']))

Configuration()
