#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Contract',
    'name_de_DE': 'Vertrag',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Contract Base
    - Provides the base functionality of contract management
''',
    'description_de_DE': '''Vertrag Basismodul
    - Stellt die Grundfunktionalitäten für die Vertragsverwaltung bereit
''',
    'depends': [
        'company_timezone',
    ],
    'xml': [
        'contract.xml',
        'configuration.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
